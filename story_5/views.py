from django.shortcuts import render, redirect
from django.urls import reverse

# Models
from .models import MataKuliah

# Form
from .forms import MataKuliahForm

def index(request):
  daftar_matkul = MataKuliah.objects.all()
  context={
    "daftar_matkul": daftar_matkul
  }
  return render(request, "story_5/index.html", context)

def create(request):
  success_notif = ''
  error_notif = ''
  if (request.method == 'POST'):
    form = MataKuliahForm(request.POST)
    if form.is_valid():
      new_matkul = MataKuliah(
        nama = request.POST['nama'],
        dosen = request.POST['dosen'],
        jumlah_sks = request.POST['jumlah_sks'],
        deskripsi = request.POST['deskripsi'],
        semester_tahun = request.POST['semester_tahun'],
        ruang_kelas = request.POST['ruang_kelas']
      )
      new_matkul.save()
      success_notif = 'Berhasil menambahkan mata kuliah!'
      return redirect('detail', pk=new_matkul.pk)
    else:
      error_notif = 'Gagal menambahkan mata kuliah'
  else:
    form = MataKuliahForm()

  context = {
    "form": form,
    "success_notif": success_notif,
    "error_notif": error_notif
  }
  return render(request, "story_5/create_matkul.html", context)

def detail(request, pk):
  matkul = MataKuliah.objects.get(pk=pk)
  context = {
    "matkul": matkul,
  }
  return render(request, "story_5/detail_matkul.html", context)

def delete(request, pk):
  matkul = MataKuliah.objects.get(pk=pk)
  matkul.delete()
  return redirect('home')




