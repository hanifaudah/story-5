from django.urls import path, include
from .views import (
  index,
  create,
  detail,
  delete
)
urlpatterns = [
    path('', index, name='home'),
    path('matkul/new/', create, name='form'),
    path('matkul/detail/<int:pk>', detail, name='detail'),
    path('matkul/delete/<int:pk>', delete, name='delete')
]