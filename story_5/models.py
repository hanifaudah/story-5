from django.db import models

class MataKuliah(models.Model):
  nama = models.CharField(max_length=128)
  dosen = models.CharField(max_length=128)
  jumlah_sks = models.IntegerField(default=0)
  deskripsi = models.TextField()
  semester_tahun = models.CharField(max_length=128)
  ruang_kelas = models.CharField(max_length=128)

  def __str__(self):
    return f'{self.nama} ({self.jumlah_sks}sks): {self.dosen}'
